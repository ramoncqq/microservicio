package com.ramon;

import com.google.common.collect.Lists;
import com.ramon.model.Student;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {DemoRamonApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DemoMongoTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoMongoTest.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void createData(){
        ArrayList<String> asignatures = Lists.newArrayList("Spring Boot", "MYSQL", "Java");
        Student student1 = new Student("Alicia", "García", 16, asignatures, "Gran via 9");
        Student student2 = new Student("Marcos", "Pérez", 14, asignatures,
                "Avenida constitución 15");
        Student student3 = new Student("Juan", "García", 25, asignatures,
                "Calle Roma 15");
        mongoTemplate.insert(student1);
        mongoTemplate.insert(student2);
        mongoTemplate.insert(student3);
    }

    @Test
    public void testFindAll() {
        LOGGER.info("FindAll - MONGO TEMPLATE:{}", mongoTemplate.findAll(Student.class));
    }

    @Test
    public void modify(){
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is("Marcos"));
        Student student = mongoTemplate.findOne(query, Student.class);
        LOGGER.info("Se ha encontrado el estudiante {}:", student);
        student.setName("Victor");
        mongoTemplate.save(student);
    }

    @Test
    public void delete(){
        Query query = new Query();
        query.addCriteria(Criteria.where("age").gte("25"));
        Student student = mongoTemplate.findOne(query, Student.class);
        LOGGER.info("Se va a eliminar el estudiante {}:", student);
        mongoTemplate.remove(student);
    }



}
