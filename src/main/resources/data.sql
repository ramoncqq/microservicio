
INSERT INTO userdb VALUES(1, 'admin@ramon.com', 'Administrador Ramón', 'admin');
INSERT INTO userdb VALUES(2, 'user@ramon.com', 'Usuario Ramón', 'user');

INSERT INTO client VALUES(1, '611234567', 'Alicia Gómez', 'Ramón');
INSERT INTO client VALUES(2, '612052103', 'Pedro García', 'Ramón');
INSERT INTO client VALUES(3, '609123421', 'Alfredo Pérez', 'Ramón');


INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion) VALUES(1, 'Aquaman', 'El poder más reconocido de Aquaman es la capacidad telepática para comunicarse con la vida marina, la cual puede convocar a grandes distancias.',
 'assets/image/aquaman.png', 'Marvel', '1941-11-01');
INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion) VALUES(2, 'Batman', 'Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.',
                         'assets/image/batman.png', 'DC', '1939-05-01');
INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion) VALUES(3, 'Daredevil', 'Al haber perdido la vista, los cuatro sentidos restantes de Daredevil fueron aumentados por la radiación a niveles superhumanos, en el accidente que tuvo cuando era niño. A pesar de su ceguera, puede \"ver\" a través de un \"sexto sentido\" que le sirve como un radar similar al de los murciélagos.',
                         'assets/image/daredevil.png', 'Marvel', '1964-01-01');
INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion)VALUES(4, 'Hulk', 'Su principal poder es su capacidad de aumentar su fuerza hasta niveles prácticamente ilimitados a la vez que aumenta su furia. Dependiendo de qué personalidad de Hulk esté al mando en ese momento (el Hulk Banner es el más débil, pero lo compensa con su inteligencia).',
                         'assets/image/hulk.png', 'Marvel', '1962-05-01');
INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion) VALUES(5, 'Linterna Verde','oseedor del anillo de poder que posee la capacidad de crear manifestaciones de luz sólida mediante la utilización del pensamiento. Es alimentado por la Llama Verde (revisada por escritores posteriores como un poder místico llamado Starheart), una llama mágica contenida en dentro de un orbe (el orbe era en realidad un meteorito verde de metal que cayó a la Tierra, el cual encontró un fabricante de lámparas llamado Chang)',
                         'assets/image/linterna-verde.png', 'DC', '1940-06-01');
INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion) VALUES(6, 'Spider-Man','Tras ser mordido por una araña radiactiva, obtuvo los siguientes poderes sobrehumanos, una gran fuerza, agilidad, poder trepar por paredes. La fuerza de Spider-Man le permite levantar 10 toneladas o más. Gracias a esta gran fuerza Spider-Man puede realizar saltos íncreibles. Un \"sentido arácnido\", que le permite saber si un peligro se cierne sobre él, antes de que suceda. En ocasiones este puede llevar a Spider-Man al origen del peligro.',
                         'assets/image/spiderman.png', 'Marvel', '1962-08-01');
INSERT INTO heroe(id, nombre, poder, imagen, casa, aparicion) VALUES(7, 'Lobezno', 'En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres . Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.',
                          'assets/image/walverine.png', 'Marvel', '1974-11-01');

