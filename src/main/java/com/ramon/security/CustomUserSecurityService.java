package com.ramon.security;
/*
import com.ramon.model.UserDB;
import com.ramon.repository.UserDbCrudRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CustomUserSecurityService implements AuthenticationProvider {

    private static Logger LOG = LoggerFactory.getLogger(CustomUserSecurityService.class);

    @Autowired
    private UserDbCrudRepository usersDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException, UsernameNotFoundException {
        Authentication auth = null;
        UserDB usersLogin = null;
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();
        LOG.info("METHOD: 'authenticate' -- login UserLogInDTO: {}, user: {}; password:{};", new Object[]{email, password});
        usersLogin = usersDao.findByEmail(email);
        if (usersLogin == null) {
            String errorMessage = String.format("METHOD: 'authenticate' -- No user found with username '%s'.", email);
            LOG.info("{}", errorMessage);
            throw new UsernameNotFoundException(errorMessage);
        } else {
            if (!password.equals(usersLogin.getPassword()))  throw new UsernameNotFoundException("Contraseña incorrecta");
            auth = new UsernamePasswordAuthenticationToken(usersLogin.getName(), passwordEncoder.encode(usersLogin.getPassword()),
                    Collections.emptyList());
            LOG.info("METHOD: 'authenticate' -- OK: : {}", auth);
            return auth;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}*/