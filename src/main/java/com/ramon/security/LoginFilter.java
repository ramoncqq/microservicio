package com.ramon.security;

/*
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ramon.model.UserDB;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    private JwtUtil jwtUtil = null;

    public LoginFilter(String url, AuthenticationManager authManager, JwtUtil jwtUtil) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException, ServletException {
        // Se obtiene el body de la solicitud
        InputStream body = req.getInputStream();
        // Se convierte la respuesta a nuestra estructura de Usuario.
        UserDB user = new ObjectMapper().readValue(body, UserDB.class);
       // Se llamará al servicio de seguridad de Spring para comprobar que los datos sean correctos
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword(),
                        Collections.emptyList()
                )
        );
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res, FilterChain chain,
            Authentication auth) throws IOException, ServletException {

        // Si la autenticacion fue exitosa, agregamos el token a la respuesta
        jwtUtil.addAuthentication(res, auth.getName());
    }
}
*/