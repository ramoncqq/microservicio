package com.ramon.security;
/*
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;

import static java.util.Collections.emptyList;

@Component
public class JwtUtil {

    @Value("${jwt.timeout}") private Long timeOut;

    @Value("${jwt.secret}") private String secret;

    // Método par crear el token
    public void addAuthentication(HttpServletResponse res, String username) {
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + timeOut))
                .signWith(SignatureAlgorithm.HS512, secret) .compact();
        // Se crea la cabecera Authorization con el token
        res.addHeader("Authorization", "Bearer " + token);
    }

    // Método para validar el token enviado por el cliente
    public Authentication getAuthentication(HttpServletRequest request) {

        // Obtenemos el token que viene en el encabezado de la peticion
        String token = request.getHeader("Authorization");

        // si hay un token presente, entonces lo validamos
        if (token != null) {
            String user = Jwts.parser().setSigningKey(this.secret)
                    .parseClaimsJws(token.replace("Bearer", ""))
                    .getBody().getSubject();
            return user != null ? new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
                    null;
        }
        return null;
    }

}*/
