package com.ramon.test;

import com.ramon.model.Article;
import com.ramon.model.Client;
import com.ramon.repository.ArticleElasticRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class ControllerTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(ControllerTest.class);

    private final static String URL_SERVICE = "http://localhost:1515/clients";

    @Autowired
    private ArticleElasticRepository articleElasticRepository;

    @GetMapping("/testGet")
    public ResponseEntity<?> getClients() {
        ResponseEntity<Client[]> response = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            response = restTemplate.getForEntity(URL_SERVICE, Client[].class);
            LOGGER.info("Código HTTP: {}", response.getStatusCode());
            LOGGER.info("Headers: {}", response.getHeaders());
            LOGGER.info("Body {}: ", response.getBody());
        } catch (Exception e) {
            LOGGER.error("Error {}: ", e);
        }

        return response;
    }

    @GetMapping("/testPost")
    public ResponseEntity<?> postClient() {
        Client newcClient = null;
        RestTemplate restTemplate = new RestTemplate();
        try {
            newcClient = new Client(null, "699123435",
                    "Prueba cliente rest", "Ibermática");
            newcClient = restTemplate.postForObject(URL_SERVICE, newcClient, Client.class);
        } catch (Exception e) {
            LOGGER.error("Error {}: ", e);
        }

        return new ResponseEntity<>(newcClient, HttpStatus.CREATED);
    }

    @PostMapping("/addElastic")
    public ResponseEntity<?> addElastic(@RequestBody Article article) {
        Article articleNEW = null;
        try {
            articleNEW = articleElasticRepository.save(article);
        } catch (Exception e) {
            LOGGER.error("Error {}: ", e);
        }
        return new ResponseEntity<>(articleNEW, HttpStatus.CREATED);
    }


    @GetMapping("/getElastic/{title}")
    public ResponseEntity<?> getElastic(@PathVariable String title) {
        Article article = null;
        try {
            article = articleElasticRepository.findByTitle(title);
        } catch (Exception e) {
            LOGGER.error("Error {}: ", e);
        }
        return new ResponseEntity<>(article, HttpStatus.CREATED);
    }

}
