package com.ramon.controller;

import com.ramon.model.Client;
import com.ramon.repository.ClientsCrudRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clients")
public class ClientsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientsController.class);

    @Autowired
    private ClientsCrudRepository clientsDAO;

    @GetMapping
    private ResponseEntity<?> getClients() {
        try {
            return new ResponseEntity<>(this.clientsDAO.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[getClients] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{phone}")
    private ResponseEntity<?> getDetailClient(@PathVariable("phone") String phone) {
        try {
            if (StringUtils.isEmpty(phone)) return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
            Client client = this.clientsDAO.findByPhone(phone);
            if (client != null) {
                return new ResponseEntity<>(this.clientsDAO.findByPhone(phone), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Cliente no existente", HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            LOGGER.error("[getClients] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    private ResponseEntity<?> createClient(@RequestBody Client client) {
        try {
            if (client == null || StringUtils.isEmpty(client.getFullname()) || StringUtils.isEmpty(client.getPhone()))
                return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
            Client newClient = this.clientsDAO.save(client);
            return new ResponseEntity<>(newClient, HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("[getClients] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{phone}")
    private ResponseEntity<?> deleteClient(@PathVariable("phone") String phone) {
        try {
            //
            if (StringUtils.isEmpty(phone)) return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
            Client client = this.clientsDAO.findByPhone(phone);
            if (client == null) {
                return new ResponseEntity<>("Cliente no existente", HttpStatus.NOT_FOUND);
            } else {
                this.clientsDAO.deleteByPhone(phone);
            }
            return new ResponseEntity<>("Cliente eliminado", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("[deleteClient] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping
    private ResponseEntity<?> modifyClient(@RequestBody Client client) {
        try {
            Client clientFind = null;
            if (client != null && client.getPhone() != null) {
                clientFind = this.clientsDAO.findByPhone(client.getPhone());
                if (clientFind != null) {
                    return new ResponseEntity<>(this.clientsDAO.save(client), HttpStatus.OK);
                } else {
                    return new ResponseEntity<>("Cliente no existe", HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<>("Cliente no existe", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error("[modifyClient] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
