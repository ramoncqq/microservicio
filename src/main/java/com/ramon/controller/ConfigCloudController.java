package com.ramon.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfigCloudController {

    @Value("${persona.nombre: TUPUTAMADRE}")
    private String name;

    @GetMapping("/")
    public String sayHello() {
        return "Hello " + name;
    }
}
