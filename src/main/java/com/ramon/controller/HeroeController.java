package com.ramon.controller;

import com.ramon.model.Heroe;
import com.ramon.repository.HeroeCrudRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/heroe")
public class HeroeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeroeController.class);

    @Autowired
    private HeroeCrudRepository heroeDAO;

    @GetMapping
    private ResponseEntity<?> getHeroes() {
        try {
            return new ResponseEntity<>(this.heroeDAO.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[getHeroes] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    private ResponseEntity<?> getHeroe(@PathVariable("id") Integer id) {
        try {
            if (id == null)
                return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
            Heroe heroe = this.heroeDAO.findOne(id);
            return new ResponseEntity<>(heroe, HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("[getHeroe] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping
    private ResponseEntity<?> createHeroe(@RequestBody @Valid Heroe heroe) {
        try {
            Heroe newHeroe = this.heroeDAO.save(heroe);
            LOGGER.info("Heroe:{}", newHeroe);
            return new ResponseEntity<>(newHeroe, HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("[createHeroe] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{id}")
    private ResponseEntity<?> deleteHeroe(@PathVariable("id") Integer id) {
        try {
            //
            if (id == null) return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
            this.heroeDAO.delete(id);
            return new ResponseEntity<>("Heroe eliminado", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("[deleteHeroe] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping
    private ResponseEntity<?> modifyHeroe(@RequestBody @Valid Heroe heroe) {
        try {
            if (heroe.getId() != null) {
                return new ResponseEntity<>(this.heroeDAO.save(heroe), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Heroe no existe", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error("[modifyHeroe] Se ha producido un error {}", e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
