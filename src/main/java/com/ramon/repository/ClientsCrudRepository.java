package com.ramon.repository;

import com.ramon.model.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ClientsCrudRepository extends CrudRepository<Client, Integer> {

    public List<Client> findAll();

    public Client save(Client client);

    public Client findByPhone(String phoneNumber);

    @Transactional
    public void deleteByPhone(String phone);

    @Query("from Client c where c.phone like (:name)")
    public Client findByPhone2(@Param("name") String nameString);

}



