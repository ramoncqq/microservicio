package com.ramon.repository;

import com.ramon.model.Article;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleElasticRepository extends ElasticsearchRepository<Article, String> {

    Article findByTitle(String name);

}
