package com.ramon.repository;

import com.ramon.model.UserDB;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDbCrudRepository extends CrudRepository<UserDB, Integer> {

    public UserDB findByEmail(String email);

}



