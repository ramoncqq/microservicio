package com.ramon.repository;

import com.ramon.model.Heroe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeroeCrudRepository extends CrudRepository<Heroe, Integer> {

    public List<Heroe> findAll();

    public Heroe save(Heroe heroe);

}



