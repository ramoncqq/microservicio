package com.ramon.repository;

import com.ramon.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentMongoRepository extends MongoRepository<Student, Integer> {
}
