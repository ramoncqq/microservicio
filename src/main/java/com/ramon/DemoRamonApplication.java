package com.ramon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class DemoRamonApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRamonApplication.class, args);
	}

}
