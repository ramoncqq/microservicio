package com.ramon.activemq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
@Slf4j
public class Listener {

    @JmsListener(destination = "micola.queue")
    @SendTo("colaSalida.queue")
    public String receiveMessage(final Message jsonMessage) throws JMSException {
        log.info("Received message " + jsonMessage);
        String response = null;
        if (jsonMessage instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) jsonMessage;
            response = "Hello " + textMessage.getText();
        }
        return response;
    }

}