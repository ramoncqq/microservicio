package com.ramon.model;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="client")
public class Client {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_id_client")
	@SequenceGenerator(name="sec_id_client", sequenceName="sec_id_client", allocationSize=0)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "phone", nullable = false)
	@NotNull
	@NotEmpty
	@Pattern(regexp = "\\d{9}", message = "El teléfono únicamente debe tener dígitos")
	private String phone;

	@Column(name = "fullname", nullable = false)
	@NotBlank
	@Size(min = 10, max = 40)
	private String fullname;

	@Column(name = "company", nullable = false)
	@NotNull
	@NotBlank
	private String company;

	// GETTERS, SETTERS Y CONSTRUCTORES


	public Client(Long id, String phone, String fullname, String company) {
		this.id = id;
		this.phone = phone;
		this.fullname = fullname;
		this.company = company;
	}

	public Client () {
		this.id = null;
		this.phone = null;
		this.fullname = null;
		this.company = null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}


	@Override
	public String toString() {
		return "Client{" +
				"id='" + id + '\'' +
				", phone='" + phone + '\'' +
				", fullname='" + fullname + '\'' +
				", company='" + company + '\'' +
				'}';
	}
}
