package com.ramon.model;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="heroe")
@Data
public class Heroe {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_id_heroe")
	@SequenceGenerator(name="sec_id_heroe", sequenceName="sec_id_heroe", allocationSize=0)
	@Column(name = "id", nullable = false)
	private Integer id;

	@Column(name = "nombre", nullable = false)
	@NotNull
	@NotEmpty
	private String nombre;

	@Column(name = "poder", nullable = false, length = 450)
	@NotBlank
	private String poder;

	@Column(name = "imagen", nullable = false)
	@NotNull
	@NotBlank
	private String imagen;

	@Column(name = "casa", nullable = false)
	@NotNull
	@NotBlank
	private String casa;

	@Column(name = "aparicion", nullable = false)
	@NotNull
	@NotBlank
	private String aparicion;

}
