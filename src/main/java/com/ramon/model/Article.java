package com.ramon.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Id;


@Document(indexName = "libreria", type = "article")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article {

    @Id
    private String id;

    private String title;

    private String author;

    private String releaseDate;

}
