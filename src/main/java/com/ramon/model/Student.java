
package com.ramon.model;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.List;

@Document(collection = "client")
public class Student {

    @Id
    private String id;

    private String name;

    private String surname;

    private Integer age;

    private List<String> subjects;

    private String street;


    // getters / setters


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Student(String name, String surname, Integer age, List<String> subjects, String street) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.subjects = subjects;
        this.street = street;
    }

    public Student(){}

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", subjects=" + subjects +
                ", street='" + street + '\'' +
                '}';
    }
}
